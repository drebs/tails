# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2021-07-21 15:20+0000\n"
"PO-Revision-Date: 2021-07-22 06:05+0000\n"
"Last-Translator: dedmoroz <cj75300@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<="
"4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 3.11.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Connecting to the Tor network\"]]\n"
msgstr "[[!meta title=\"Подключение к сети Tor\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Plain text
msgid ""
"Everything you do on the Internet from Tails goes through the Tor network."
msgstr "Всё, что вы делаете онлайн в Tails, проходит через сеть Tor."

#. type: Plain text
msgid ""
"Tor encrypts and anonymizes your connection by passing it through 3 relays. "
"Tor relays are servers operated by different people and organizations around "
"the world."
msgstr ""
"Tor шифрует и анонимизирует ваш трафик, пропуская его через три узла. Это "
"серверы, которые поддерживают разные люди и организации по всему миру."

#. type: Plain text
#, no-wrap
msgid "[[!img doc/about/warnings/htw2-tails.png link=\"no\"]]\n"
msgstr "[[!img doc/about/warnings/htw2-tails.png link=\"no\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"You cannot access the Internet from Tails until you are connected to Tor. For\n"
"example, *Tor Browser* displays the error message <span class=\"code\">Proxy\n"
"server refusing connections</span> until you are connected to Tor.\n"
msgstr "Из Tails нельзя попасть в Интернет, минуя Tor. Например, *Tor Browser* будет показывать ошибку <span class=\"code\">отказа в подключении от прокси-сервера</span>, пока вы не подключитесь к Tor.\n"

#. type: Plain text
msgid "To connect to the Tor network:"
msgstr "Как подключиться к сети Tor."

#. type: Bullet: '1. '
msgid "[[Connect to a local network|networkmanager]], wired, Wi-Fi, or mobile."
msgstr ""
"[[Подключитесь к сети|networkmanager]] по проводу, wi-fi или мобильной связи."

#. type: Bullet: '2. '
msgid ""
"The *Tor Connection* assistant appears to help you connect to the Tor "
"network."
msgstr "Появится *мастер подключения Tor*. Он поможет подключиться к сети Tor."

#. type: Plain text
#, no-wrap
msgid "   [[!img tor-connection.png link=\"no\"]]\n"
msgstr "   [[!img tor-connection.png link=\"no\"]]\n"

#. type: Bullet: '3. '
msgid "Choose whether you want to:"
msgstr "Выберите один из вариантов:"

#. type: Bullet: '   - '
msgid "Connect to Tor automatically"
msgstr "подключиться к Tor автоматически;"

#. type: Bullet: '   - '
msgid "Hide to your local network that you are connecting to Tor"
msgstr "скрыть от вашей локальной сети факт подключения к Tor."

#. type: Plain text
#, no-wrap
msgid "   The implications of both methods are detailed below.\n"
msgstr "   Далее мы рассказываем об особенностях каждого метода.\n"

#. type: Title =
#, no-wrap
msgid "Tor relays and bridges"
msgstr "Узлы и мосты Tor"

#. type: Plain text
msgid "- **Public Tor relays**"
msgstr "- **Публичные узлы Tor**"

#. type: Plain text
#, no-wrap
msgid ""
"  Most of the time, your local network does not block access to the Tor network\n"
"  and you can use a public relay as your first Tor relay.\n"
msgstr "  Скорее всего, ваша локальная сеть не блокирует доступ к сети Tor. Тогда вы можете использовать в качестве первого узла Tor открытый узел.\n"

#. type: Plain text
#, no-wrap
msgid ""
"  Using a public Tor relay as your first Tor relay makes it clear to your local\n"
"  network that you are connecting to Tor, while still keeping your online\n"
"  activity secure and anonymous.\n"
msgstr "  Когда для подключения к сети Tor вы используете публичный узел Tor, ваша локальная сеть «знает», что вы соединяетесь с Tor (хотя все ваши действия онлайн по-прежнему анонимны).\n"

#. type: Plain text
msgid "- **Tor bridges**"
msgstr "- **Мосты Tor**"

#. type: Plain text
#, no-wrap
msgid ""
"  Tor bridges are secret Tor relays that keep your connection to the Tor\n"
"  network hidden.\n"
msgstr ""
"  Мосты Tor — секретные узлы Tor, которые помогают \n"
"  скрыть факт вашего подключения к сети Tor.\n"

#. type: Plain text
#, no-wrap
msgid ""
"  Use a bridge as your first Tor relay if connecting to Tor is blocked or if\n"
"  using Tor could look suspicious to someone who monitors your Internet\n"
"  connection.\n"
msgstr "  Если подключение к Tor заблокировано или может вызвать нездоровый интерес у кого-то, кто способен мониторить ваши сетевые подключения, попробуйте использовать мост в качестве первого узла в цепочке Tor.\n"

#. type: Plain text
#, no-wrap
msgid ""
"  The technology used by Tor bridges is designed to circumvent censorship\n"
"  where connections to Tor are blocked, for example in some countries with heavy censorship,\n"
"  by some public networks, or by some parental controls.\n"
msgstr "  Мосты Tor созданы для обхода блокировки Tor: например, в странах, где подключения к сети Tor блокируются, в некоторых публичных сетях и системах родительского контроля.\n"

#. type: Plain text
#, no-wrap
msgid ""
"  It does so by camouflaging your connection so it cannot be recognized as a\n"
"  connection to Tor. As a consequence, the same technology can be used to hide\n"
"  that you are using Tor if it could look suspicious to someone who monitors\n"
"  your Internet connection.\n"
msgstr "  Ваше подключение имеет такой вид, что никто не заподозрит связь с Tor.\n"

#. type: Plain text
#, no-wrap
msgid "  Tor bridges are often less reliable and slower than public Tor relays.\n"
msgstr "  Мосты Tor часто оказываются менее надёжными и быстрыми, чем публичные узлы Tor.\n"

#. type: Title =
#, no-wrap
msgid "Connecting to Tor automatically"
msgstr "Автоматическое подключение к Tor"

#. type: Plain text
msgid ""
"We recommend connecting to Tor automatically if you are on a public Wi-Fi "
"network or if many people in your country use Tor to circumvent censorship."
msgstr ""
"Советуем подключаться к Tor автоматически, если вы находитесь в публичной "
"сети wi-fi или если множество людей в вашей стране использует Tor для обхода "
"цензуры."

#. type: Plain text
msgid ""
"When you choose this option, Tails tries different ways of connecting to Tor "
"until it succeeds:"
msgstr ""
"При выборе этой опции Tails пробует разные варианты подключения к Tor, пока "
"не добьётся успеха."

#. type: Bullet: '1. '
msgid ""
"Tails tries to connect to Tor directly using **public relays**, without "
"using bridges."
msgstr ""
"Tails  старается подключиться к Tor напрямую через **публичные узлы** без "
"использования мостов."

#. type: Bullet: '1. '
msgid ""
"Tails tries to connect to Tor using a set of **default bridges**, already "
"included in Tails, if connecting using public relays fails."
msgstr ""
"Если через публичные узлы не получится, Tails попробует подключиться к Tor с "
"помощью набора **мостов по умолчанию**, которые уже включены в Tails."

#. type: Bullet: '1. '
msgid ""
"Tails asks you to configure **custom bridges**, if connecting using the "
"default bridges fails."
msgstr "Если и это не получится, Tails попросит вас **указать мосты вручную**."

#. type: Plain text
msgid ""
"Someone monitoring your Internet connection could identify these attempts as "
"coming from a Tails user."
msgstr ""
"Если кто-то мониторит ваше подключение к Интернету, он может определить, что "
"эти попытки исходят от пользователя Tails."

#. type: Plain text
msgid ""
"If connecting to Tor automatically fails, the *Tor Connection* assistant "
"helps you:"
msgstr ""
"Если автоматически подключиться к Tails не удалось, воспользуйтесь "
"помощником по соединению с Tor."

#. type: Plain text
msgid ""
"- [[Sign in to the network using a captive portal|unsafe_browser]] - "
"Configure a local proxy"
msgstr ""
"- [[Войдите в сеть через сайт авторизации|unsafe_browser]] — настройте "
"локальный прокси"

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid "<p>In the future, Tails will also automatically:</p>\n"
msgstr "<p>В будущем Tails также будет автоматически:</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<ul>\n"
"<li>Detect if you have to sign in to the local network using a captive portal ([[!tails_ticket 5785]])</li>\n"
"<li>Synchronize the clock of the computer to make it easier to use Tor bridges in Asia ([[!tails_ticket 15548]])</li>\n"
"</ul>\n"
msgstr ""
"<ul>\n"
"<li>определять, нужно ли вам для входа в сеть использовать сайт авторизации ([[!tails_ticket 5785]]);</li>\n"
"<li>синхронизировать время на компьютере, чтобы было проще использовать мосты Tor в азиатских странах ([[!tails_ticket 15548]]).</li>\n"
"</ul>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Title =
#, no-wrap
msgid "Hiding to your local network that you are connecting to Tor"
msgstr "Как скрыть факт использования Tor от локальной сети"

#. type: Plain text
msgid ""
"You might need to go unnoticed if using Tor could look suspicious to someone "
"who monitors your Internet connection."
msgstr ""
"Что, если злоумышленник мониторит ваше подключение к Интернету, а вам нужно "
"подключиться к Tor незаметно и не вызывая подозрений?"

#. type: Plain text
msgid ""
"When you choose this option, Tails will only connect to Tor after you "
"configure Tor bridges. Bridges are secret Tor relays that hide that you are "
"connecting to Tor."
msgstr ""
"При выборе этой настройки Tails будет подключаться к Tor только при "
"настроенных мостах. Мосты — это секретные узлы Tor, которые скрывают сам "
"факт вашего подключения к Tor. Их нужно настроить."

#. type: Plain text
msgid ""
"Our team is doing its best to help you connect to Tor using the most "
"discrete types of Tor bridges. That is why, when you decide to hide that you "
"are connecting to Tor:"
msgstr ""
"Наша команда разработчиков делает всё возможное, чтобы обеспечить вас самыми "
"малозаметными мостами. Что же будет, если вы выберете этот вариант "
"подключения к Tor?"

#. type: Plain text
#, no-wrap
msgid "<!-- Mention captive portal detection here once we have #5785 -->\n"
msgstr "<!-- Mention captive portal detection here once we have #5785 -->\n"

#. type: Plain text
msgid "- Default bridges are not available."
msgstr "- Мосты по умолчанию недоступны."

#. type: Plain text
#, no-wrap
msgid "  You will have to know the address of custom bridges.\n"
msgstr "  Нужно знать адрес хотя бы одного моста для настройки вручную.\n"

#. type: Plain text
#, no-wrap
msgid "  To request custom bridges, you can either:\n"
msgstr "  Получить мосты можно одним из двух способов.\n"

#. type: Bullet: '  1. '
msgid ""
"Request bridges on [https://bridges.torproject.org/](https://bridges."
"torproject.org/bridges?transport=obfs4)."
msgstr ""
"Запросить мосты по адресу [https://bridges.torproject.org/](https://bridges."
"torproject.org/bridges?transport=obfs4)."

#. type: Plain text
#, no-wrap
msgid ""
"     We recommend doing so before starting Tails and ideally from a different\n"
"     local network than the one on which you want to hide that you are using\n"
"     Tor.\n"
msgstr ""
"     Советуем сделать это до запуска Tails, и ещё лучше — из другой \n"
"     локальной сети, не той, от которой вы хотите скрыть факт использования \n"
"     Tor.\n"

#. type: Bullet: '  1. '
msgid ""
"Send an empty email to <a href=\"mailto:bridges@torproject.org"
"\">bridges@torproject.org</a> from a Gmail or Riseup email address."
msgstr ""
"Отправить пустое сообщение по адресу <a href=\"mailto:bridges@torproject.org"
"\">bridges@torproject.org</a> с ящика Gmail или Riseup."

#. type: Plain text
#, no-wrap
msgid ""
"     Sending such an email, from your phone for example, does not reveal to\n"
"     your local network that you are trying to connect to Tor.\n"
msgstr ""
"     Если вы отправите такое сообщение, например, с мобильного телефона, \n"
"     ваша локальная сеть не узнает, что вы хотите подключиться к Tor.\n"

#. type: Plain text
msgid ""
"- You can only use the types of bridges that our team considers discrete "
"enough."
msgstr ""
"- Вы можете использовать только те виды мостов, которые наша команда считает "
"достаточно незаметными."

#. type: Plain text
#, no-wrap
msgid "  Currently in Tails, only **obfs4 bridges** hide that you are using Tor.\n"
msgstr "  В настоящее время для сокрытия факта использования Tor в Tails подходят только **мосты obfs4**.\n"

#. type: Plain text
#, no-wrap
msgid "  obfs4 bridges look like:\n"
msgstr "  Мосты obfs4 выглядят так:\n"

#. type: Plain text
#, no-wrap
msgid "      obfs4 1.2.3.4:1234 B0E566C9031657EA7ED3FC9D248E8AC4F37635A4 cert=OYWq67L7MDApdJCctUAF7rX8LHvMxvIBPHOoAp0+YXzlQdsxhw6EapaMNwbbGICkpY8CPQ iat-mode=0\n"
msgstr ""
"      obfs4 1.2.3.4:1234 B0E566C9031657EA7ED3FC9D248E8AC4F37635A4 "
"cert=OYWq67L7MDApdJCctUAF7rX8LHvMxvIBPHOoAp0+YXzlQdsxhw6EapaMNwbbGICkpY8CPQ "
"iat-mode=0\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>The entire line needs to be entered, not just the IP address and port\n"
"combination.</p>\n"
msgstr "<p>Нужно вводить всю строчку, не только IP-адрес и порт.</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>It is impossible to hide to the websites that you visit that you are using\n"
"Tor, because the <a href=\"https://metrics.torproject.org/exonerator.html\">list\n"
"of exit nodes of the Tor network is public</a>.</p>\n"
msgstr ""
"<p>Нельзя скрыть от посещаемых вами сайтов то, что вы используете Tor, \n"
"потому что <a href=\"https://metrics.torproject.org/exonerator.html\">список выходных узлов Tor открыт</a>.</p>\n"

#. type: Plain text
msgid ""
"If connecting to Tor using custom bridges fails, the *Tor Connection* "
"assistant helps you:"
msgstr ""
"Если подключение к Tor через настроенные вручную мосты не удалось, "
"попробуйте действовать через помощника подключения к Tor."

#. type: Plain text
#, no-wrap
msgid "<p>In the future, we will make it easier to use custom bridges by:</p>\n"
msgstr "<p>В перспективе использовать вручную настроенные мосты станет проще:</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<ul>\n"
"<li>Allow you to save custom bridges in the Persistent Storage ([[!tails_ticket 5461]])</li>\n"
"<li>Allow scanning the QR code returned by <a href=\"mailto:bridges@torproject.org\">bridges@torproject.org</a> ([[!tails_ticket 18219]])</li>\n"
"<li>Allow requesting bridges from Tails by solving a CAPTCHA ([[!tails_ticket 15331]])</li>\n"
"<li>Synchronize the clock of the computer to be able to connect to Tor bridges more easily ([[!tails_ticket 15548]])</li>\n"
"</ul>\n"
msgstr ""
"<ul>\n"
"<li>вы сможете сохранять настроенные вами мосты в Постоянном хранилище ([[!tails_ticket 5461]]);</li>\n"
"<li>с адреса <a href=\"mailto:bridges@torproject.org\">bridges@torproject.org</a> будет приходить пригодный для сканирования QR-код ([[!tails_ticket 18219]]);</li>\n"
"<li>можно будет запрашивать мосты из Tails, попутно решая капчу ([[!tails_ticket 15331]]);</li>\n"
"<li>будет синхронизированы часы на компьютере, что упростит подключение к мостам Tor ([[!tails_ticket 15548]]).</li>\n"
"</ul>\n"

#. type: Title =
#, no-wrap
msgid "Troubleshooting Tor bridges"
msgstr "Решение проблем с мостами Tor"

#. type: Plain text
msgid ""
"If the bridges that you entered do not work, try requesting another set of "
"bridges. It is possible that the bridges you entered are no longer "
"operational."
msgstr ""
"Если указанные вами мосты не работают, попробуйте запросить другие мосты. "
"Возможно, указанные вами мосты уже не функционируют."

#. type: Plain text
#, no-wrap
msgid "<div class=\"bug\">\n"
msgstr "<div class=\"bug\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>When connecting to Tor, Tails sets the system time to the current time\n"
"in the [[!wikipedia Coordinated_Universal_Time]] (UTC) timezone.</p>\n"
msgstr ""
"<p>При подключении к Tor Tails устанавливает системное время в соответствии "
"с текущим временем [[!wikipedia Coordinated_Universal_Time]] (UTC).</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>Many operating systems, such as Linux and macOS, write time expressed\n"
"in UTC to the hardware clock of the computer. But, Windows instead\n"
"writes time expressed in the local timezone to the hardware clock of\n"
"the computer.</p>\n"
msgstr ""
"<p>Многие операционные системы, такие как Linux и macOS, настраивают часы "
"вашего компьютера на время UTC. Windows настраивает часы компьютера на "
"местный часовой пояс.</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>So, if you are east of the United Kingdom (which is in the UTC timezone) on\n"
"a computer that also runs Windows, Tails will make the system clock go\n"
"backwards during startup. Unfortunately, this can prevent bridges from working.</p>\n"
msgstr ""
"<p>Если вы находитесь к востоку от Великобритании (UTC-время) и работаете на "
"компьютере с Windows, Tails при запуске сдвинет ваше системное время назад. "
"К сожалению, это может помешать работе мостов Tor.</p>\n"

#. type: Plain text
#, no-wrap
msgid "<p>We are working on a fix for this issue. See [[!tails_ticket 15548]].</p>\n"
msgstr ""
"<p>Мы работаем над исправлением этой ошибки. ([[!tails_ticket 15548]])</p>\n"

#. type: Title =
#, no-wrap
msgid "Viewing the status of Tor"
msgstr "Как увидеть статус Tor"

#. type: Plain text
msgid "The status of Tor appears as an onion icon in the notification area:"
msgstr "Статус Tor виден как значок луковицы в области уведомлений:"

#. type: Plain text
#, no-wrap
msgid "[[!img doc/first_steps/introduction_to_gnome_and_the_tails_desktop/tor-status.png link=\"no\"]]\n"
msgstr "[[!img doc/first_steps/introduction_to_gnome_and_the_tails_desktop/tor-status.png link=\"no\"]]\n"

#. type: Bullet: '  - '
msgid ""
"[[!img lib/symbolic/tor-connected.png alt=\"Onion icon\" link=no "
"class=symbolic]] You are connected to Tor."
msgstr ""
"[[!img lib/symbolic/tor-connected.png alt=\"Значок луковицы\" link=no "
"class=symbolic]] Вы подключены к Tor."

#. type: Bullet: '  - '
msgid ""
"[[!img lib/symbolic/tor-disconnected.png alt=\"Onion icon that is crossed out"
"\" link=no class=\"symbolic\"]] You are not connected to Tor."
msgstr ""
"[[!img lib/symbolic/tor-disconnected.png alt=\"Значок луковицы зачёркнут\" "
"link=no class=\"symbolic\"]] Вы не подключены к Tor."

#~ msgid ""
#~ "  obfs4 bridges start with the word <span class=\"code\">obfs4</span>.\n"
#~ msgstr ""
#~ "  Название моста obfs4 всегда начинается с <span class=\"code\">obfs4</"
#~ "span>.\n"
