# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-02-22 11:27+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
msgid "If your Wi-Fi interface is not working, either:"
msgstr ""

#. type: Bullet: '* '
msgid "There is no Wi-Fi option in the system menu:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img system-without-wi-fi.png link=\"no\" alt=\"\"]]\n"
msgstr ""

#. type: Bullet: '* '
msgid ""
"The interface is disabled when starting Tails or when plugging in your USB "
"Wi-Fi adapter:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img mac-spoofing-disabled.png link=\"no\" alt=\"Notification about network card being disabled\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  In this case, you can disable MAC spoofing to get your Wi-Fi interface\n"
"  to work in Tails. Disabling MAC spoofing has security implications, so read\n"
"  carefully our [[documentation about MAC\n"
"  spoofing|first_steps/startup_options/mac_spoofing]] before doing so.\n"
msgstr ""

#. type: Plain text
msgid "To connect to the Internet, you can try to either:"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Use an Ethernet cable instead of Wi-Fi if possible. Wired interfaces work "
"much more reliably than Wi-Fi in Tails."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"wi-fi-adapters\"></a>\n"
msgstr ""

#. type: Bullet: '* '
msgid "Buy a USB Wi-Fi adapter that works in Tails:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  <table>\n"
"  <tr><th>Vendor</th><th>Model</th><th>Size</th><th>Speed</th><th>Price</th><th>Buy offline</th><th>Buy online</th></tr>\n"
"  <tr><td>Edimax</td><td>EW-7811Un</td><td>Nano</td><td>150 Mbit/s</td><td>$10</td><td>No</td><td><a href=\"https://www.amazon.com/d/B003MTTJOY/\">Amazon</a></td></tr>\n"
"  <tr><td>Panda Wireless</td><td>Ultra</td><td>Nano</td><td>150 Mbit/s</td><td>$12</td><td>No</td><td><a href=\"https://www.amazon.com/d/B00762YNMG/\">Amazon</a></td></tr>\n"
"  <tr><td>Panda Wireless</td><td>PAU05</td><td>Small</td><td>300 Mbit/s</td><td>$14</td><td>No</td><td><a href=\"https://www.amazon.com/d/B00EQT0YK2/\">Amazon</a></td></tr>\n"
"  <!--\n"
"  The following USB adapters work as well but don't really have advantages over the ones that are listed already:\n"
"  <tr><td>Panda Wireless</td><td>Mini</td><td>Large</td><td>150 Mbit/s</td><td>$10</td><td>No</td><td><a href=\"https://www.amazon.com/d/B003283M6Q/\">Amazon</a></td></tr>\n"
"  <tr><td>Panda Wireless</td><td>N600</td><td>Large</td><td>300 Mbit/s</td><td>$25</td><td>No</td><td><a href=\"https://www.amazon.com/d/B00U2SIS0O/\">Amazon</a></td></tr>\n"
"  <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>\n"
"  -->\n"
"  </table>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  <div class=\"note\">\n"
"  <p>If you find another USB Wi-Fi adapter that works in Tails, please let us\n"
"  know. You can write to [[tails-testers@boum.org|about/contact#tails-testers]]\n"
"  (public mailing list) or [[tails@boum.org|about/contact#tails]] (private\n"
"  email).</p>\n"
"  </div>\n"
msgstr ""
