# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-05-07 10:27+0000\n"
"PO-Revision-Date: 2015-10-16 15:08+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/news/fa/"
">\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Content of: outside any tag (error?)
#, fuzzy
#| msgid "[[!meta title=\"News\"]] [[!meta robots=\"noindex\"]]"
msgid ""
"[[!meta title=\"Welcome to Tails!\"]] [[!meta stylesheet=\"home\" rel="
"\"stylesheet\" title=\"\"]] [[!meta robots=\"noindex\"]] [[!meta script="
"\"home\"]]"
msgstr "[[!meta title=\"اخبار\"]] [[!meta robots=\"noindex\"]]"

#.  Note for translators: You can use <span class="twolines">
#.  if your
#. translation of the label below is long and gets split into two lines. 
#. type: Content of: <div>
#, fuzzy
#| msgid ""
#| "<a href=\"https://check.torproject.org/\"> [[!img \"lib/onion.png\" link="
#| "\"no\"]] <span>Tor check</span> </a>"
msgid ""
"<a href=\"https://tails.boum.org/install/check/\"> [[!img \"lib/onion.png\" "
"link=\"no\"]] <span>Tor check</span> </a>"
msgstr ""
"<a href=\"https://check.torproject.org/\"> [[!img \"lib/onion.png\" link=\"no"
"\"]] <span>تست تور</span> </a>"

#. type: Content of: <a>
msgid ""
"<a id=\"donate\" class=\"random-message\" href=\"https://tails.boum.org/"
"donate?r=h\" data-display-probability=\"0.1\">"
msgstr ""

#. type: Content of: <a><p>
msgid "Tails needs donations from users like you."
msgstr ""

#. type: Content of: <a><p>
msgid ""
"But, not everyone can donate. When you donate, you are offering to many "
"others who need it, this precious tool that is Tails."
msgstr ""

#. type: Content of: <a><p>
msgid "Donate"
msgstr ""

#. type: Content of: outside any tag (error?)
msgid "</a> [[!inline pages=\"news\" raw=\"yes\" sort=\"age\"]]"
msgstr "</a> [[!inline pages=\"news.fa\" raw=\"yes\" sort=\"age\"]]"
